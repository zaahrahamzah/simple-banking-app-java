import java.util.Scanner;

public class BankAccount {
	int balance;
	int previousTransaction;
	String customerName;
	String customerID;
	
	public BankAccount(String cName, String cID) {
		customerName = cName;
		customerID = cID;
	}

	void deposit (int amount) {
		if (amount != 0) {
			balance = balance + amount;
			previousTransaction = amount;
		}
	}
	
	void withdraw(int amount) {
		if (amount < balance) {
			balance = balance - amount;
			previousTransaction = - amount;
			
		} else {
			System.out.println("Insufficient Balance");
		}
	}
	
	void getPreviousTransaction() {
		
		if(previousTransaction > 0) {
			
			System.out.println("Deposited: " + previousTransaction);
			
		} else if (previousTransaction < 0) {
			
			System.out.println("Withdrawn: " + Math.abs(previousTransaction));
		
		} else {
			
			System.out.println("No transaction occured");
		}		
	}
	
	public void showMenu () {
		
		char option = '\0';
		Scanner scanner = new Scanner(System.in);
				
		System.out.println("Welcome " + customerName);
		System.out.println("Your ID is " + customerID);
		System.out.println("\n");
		System.out.println("A. Check Balance");
		System.out.println("B. Deposit");
		System.out.println("C. Withdraw");
		System.out.println("D. Previous Transaction");
		System.out.println("E. Exit");
		
		do {
				System.out.print("\n**Enter an option: ");
				option = scanner.next().charAt(0);
				
				switch(option) {
				
				case 'A':
					System.out.println("---------------");
					System.out.println("Balance = " + balance);
					System.out.println("---------------");
					break;
				
				case 'B':
					System.out.println("---------------");
					System.out.print("Enter amount to be deposited: " );
					int depositAmount = scanner.nextInt(10);
					deposit(depositAmount);
					break;
				
				case 'C':
					System.out.println("---------------");
					System.out.print("Enter amount to be withdrawn: ");
					int withdrawnAmount = scanner.nextInt();
					withdraw(withdrawnAmount);					
					break;
					
				case 'D':
					System.out.println("---------------");
					getPreviousTransaction();
					break;
					
				case 'E':
					System.out.println("---------------");
					break;
		
				} 
		} while (option != 'E');
		
		System.out.println("Thank you for using our service.");
	}	

}


