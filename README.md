# Simple Banking App - Java 

Following a tutorial in youTube, improvising a bit - https://www.youtube.com/watch?v=wQbEH4tVMJA

+ This simple app can 
    - Check Balance
    - Deposit Money
    - Withdraw
    - Show previous transaction

- This app did not store any data 
